<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;
use app\models\Lead;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadid
 * @property string $name
 * @property integer $amount
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['leadid', 'name', 'amount'], 'required'],
            [['leadid', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadid' => 'Leadid',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }
	public static function getLeads()
	{
		$allLeads = self::find()->all();
		$allLeadsArray = ArrayHelper::
				map($allLeads, 'id', 'name');
	return $allLeadsArray;						
	}
	public function getLeadItem()
	{
		return $this->hasOne(Lead::className(), ['id' => 'leadid']);
	}
	/*public function getLead()
    {
        return $this->hasOne(Lead::className(), ['id' => 'leadid']);
    }*/
	
	public function getDealItem()
    {
	   
		return $this->hasOne(Lead::className(), ['id' => 'leadid']);   
	}
	
	public static function getDeals()  
	{		
		$allDeals = self::find()->all();
		$allDealsArray = ArrayHelper::
				map($allDeals, 'id', 'name');
		return $allDealsArray;
	}
	
	
    /**
     * @inheritdoc
     * @return DealQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DealQuery(get_called_class());
    }
	

	
}
